Rails.application.routes.draw do
  resources :books
  # For details on the DSL available within this file, see http://guides.rubyonrails
  #root 'application#hello'
  root 'books#index'
end
